<?php
/**
 * @var $installer Mage_Core_Model_Resource_Setup
 */
$installer = $this;

/**
 * Creating table overdose_testimonials
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('overdose_testimonials/testimonials'))
    ->addColumn('testimonials_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
        'unsigned' => true,
        'identity' => true,
        'nullable' => false,
        'primary'  => true,
    ], 'Entity id')
    ->addColumn('title', Varien_Db_Ddl_Table::TYPE_TEXT, 255, [
        'nullable' => true,
    ], 'Title')
    ->addColumn('message', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', [
        'nullable' => true,
        'default'  => null,
    ], 'Message')
    ->addColumn('image', Varien_Db_Ddl_Table::TYPE_TEXT, null, [
        'nullable' => true,
        'default'  => null,
    ], 'Testimonials image media path')
    ->setComment('Testimonials item');

$installer->getConnection()->createTable($table);
