<?php

class Overdose_Testimonials_Adminhtml_TestimonialsController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Init actions
     *
     * @return Overdose_Testimonials_Adminhtml_TestimonialsController
     */
    protected function _initAction()
    {
        $helper = Mage::helper('overdose_testimonials');

        $this->loadLayout()
            ->_setActiveMenu('testimonials/manage')
            ->_addBreadcrumb(
                  $helper->__('Testimonials'),
                  $helper->__('Testimonials')
              )
            ->_addBreadcrumb(
                  $helper->__('Manage Testimonials'),
                  $helper->__('Manage Testimonials')
              )
        ;
        return $this;
    }

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->_title($this->__('Testimonials'))
             ->_title($this->__('Manage Testimonials'));

        $this->_initAction();
        $this->renderLayout();
    }

    /**
     * Create testimonials item
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * Edit News item
     */
    public function editAction()
    {
        $helper = Mage::helper('overdose_testimonials');
        $this->_title($this->__('Testimonials'))
             ->_title($this->__('Manage testimonials'));

        $model = Mage::getModel('overdose_testimonials/testimonials');

        $newsId = $this->getRequest()->getParam('id');
        if ($newsId) {
            $model->load($newsId);

            if (!$model->getId()) {
                $this->_getSession()->addError(
                    $helper->__('Testimonials item does not exist.')
                );
                return $this->_redirect('*/*/');
            }

            $this->_title($model->getTitle());
            $breadCrumb = $helper->__('Edit Item');
        } else {
            $this->_title($helper->__('New Item'));
            $breadCrumb = $helper->__('New Item');
        }

        $this->_initAction()->_addBreadcrumb($breadCrumb, $breadCrumb);
        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);

        if (!empty($data)) {
            $model->addData($data);
        }

        Mage::register('testimonials_item', $model);
        $this->renderLayout();
    }

    /**
     * Save action
     */
    public function saveAction()
    {
        $redirectPath   = '*/*';
        $redirectParams = [];
        $helper = Mage::helper('overdose_testimonials');
        $data = $this->getRequest()->getPost();

        if ($data) {
            $data = $this->_filterPostData($data);
            $model = Mage::getModel('overdose_testimonials/testimonials');
            $testimonialsId = $this->getRequest()->getParam('testimonials_id');

            if ($testimonialsId) {
                $model->load($testimonialsId);
            }

            if (isset($data['image'])) {
                $imageData = $data['image'];
                unset($data['image']);
            } else {
                $imageData = [];
            }
            $model->addData($data);

            try {
                $hasError = false;
                $imageHelper = Mage::helper('overdose_testimonials/image');

                if (isset($imageData['delete']) && $model->getImage()) {
                    $imageHelper->removeImage($model->getImage());
                    $model->setImage(null);
                }

                $imageFile = $imageHelper->uploadImage('image');

                if ($imageFile) {
                    if ($model->getImage()) {
                        $imageHelper->removeImage($model->getImage());
                    }
                    $model->setImage($imageFile);
                }

                $model->save();
                $this->_getSession()->addSuccess(
                    $helper->__('The testimonials item has been saved.')
                );

                if ($this->getRequest()->getParam('back')) {
                    $redirectPath   = '*/*/edit';
                    $redirectParams = ['id' => $model->getId()];
                }
            } catch (Mage_Core_Exception $e) {
                $hasError = true;
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $hasError = true;
                $this->_getSession()->addException($e,
                    $helper->__('An error occurred while saving the testimonials item.')
                );
            }

            if ($hasError) {
                $this->_getSession()->setFormData($data);
                $redirectPath   = '*/*/edit';
                $redirectParams = ['id' => $this->getRequest()->getParam('id')];
            }
        }

        $this->_redirect($redirectPath, $redirectParams);
    }

    /**
     * Delete action
     */
    public function deleteAction()
    {
        $helper = Mage::helper('overdose_testimonials');
        $itemId = $this->getRequest()->getParam('id');

        if ($itemId) {
            try {
                $model = Mage::getModel('overdose_testimonials/testimonials');
                $model->load($itemId);
                if (!$model->getId()) {
                    Mage::throwException($helper->__('Unable to find a testimonialss item.'));
                }
                $model->delete();

                $this->_getSession()->addSuccess(
                    $helper->__('Thetestimonialsitem has been deleted.')
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addException($e,
                    $helper->__('An error occurred while deleting thetestimonialsitem.')
                );
            }
        }

        $this->_redirect('*/*/');
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        $actionName = $this->getRequest()->getActionName();
        $session = Mage::getSingleton('admin/session');

        if ($actionName === 'new' || $actionName === 'save') {
            return $session->isAllowed('testimonials/manage/save');
        } elseif ($actionName === 'delete') {
            $actionName->isAllowed('testimonials/manage/delete');
        }

        return $session->isAllowed('testimonials/manage');
    }

    /**
     * Filtering posted data. Converting localized data if needed
     *
     * @param array
     * @return array
     */
    protected function _filterPostData($data)
    {
        $data = $this->_filterDates($data, ['time_published']);
        return $data;
    }

    /**
     * Grid ajax action
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Flush News Posts Images Cache action
     */
    public function flushAction()
    {
        if (Mage::helper('overdose_testimonials/image')->flushImagesCache()) {
            $this->_getSession()->addSuccess('Cache successfully flushed');
        } else {
            $this->_getSession()->addError('There was error during flushing cache');
        }
        $this->_forward('index');
    }
}