 <?php

class Overdose_Testimonials_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Pre dispatch action that allows to redirect to no route page in case of disabled extension through admin panel
     */
    public function preDispatch()
    {
        parent::preDispatch();
        
        if (!Mage::helper('overdose_testimonials')->isEnabled()) {
            $this->setFlag('', 'no-dispatch', true);
            $this->_redirect('noRoute');
        }        
    }
    
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->loadLayout();

        $listBlock = $this->getLayout()->getBlock('news.list');

        if ($listBlock) {
            $currentPage = abs(intval($this->getRequest()->getParam('p')));
            if ($currentPage < 1) {
                $currentPage = 1;
            }
            $listBlock->setCurrentPage($currentPage);
        }

        $this->renderLayout();
    }

    /**
     * News view action
     */
    public function viewAction()
    {
        $newsId = $this->getRequest()->getParam('id');
        if (!$newsId) {
            return $this->_forward('noRoute');
        }

        /** @var $model Overdose_Testimonials_Model_Testimonials */
        $model = Mage::getModel('overdose_testimonials/testimonials');
        $model->load($newsId);

        if (!$model->getId()) {
            return $this->_forward('noRoute');
        }

        Mage::register('testimonials_item', $model);
        
        Mage::dispatchEvent('before_news_item_display', ['testimonials_item' => $model]);

        $this->loadLayout();
        $itemBlock = $this->getLayout()->getBlock('testimonials.item');
        if ($itemBlock) {
            $listBlock = $this->getLayout()->getBlock('testimonials.list');
            if ($listBlock) {
                $page = (int)$listBlock->getCurrentPage() ? (int)$listBlock->getCurrentPage() : 1;
            } else {
                $page = 1;
            }
            $itemBlock->setPage($page);
        }
        $this->renderLayout();
    }

    /**
     * @return Mage_Adminhtml_Model_Session|Mage_Core_Model_Abstract|null
     */
    protected function getSession()
    {
        return Mage::getSingleton('customer/session');
    }

    public function saveAction()
    {
        $redirectPath   = '*/*';
        $helper = Mage::helper('overdose_testimonials');
        $data = $this->getRequest()->getPost();

        if (!$this->_validateFormKey()) {
            $this->_redirect($redirectPath);
            $this->getSession()->addError($helper->__('An error occurred while saving the testimonials item.'));

            return;
        }

        if ($data) {
            $model = Mage::getModel('overdose_testimonials/testimonials');

            if (isset($data['image'])) {
                $imageData = $data['image'];
                unset($data['image']);
            } else {
                $imageData = [];
            }
            $model->addData($data);

            try {
                $imageHelper = Mage::helper('overdose_testimonials/image');
                $imageFile = $imageHelper->uploadImage('image');

                if ($imageFile) {
                    $model->setImage($imageFile);
                }

                $model->save();
                $this->getSession()->addSuccess(
                    $helper->__('The testimonials item has been saved.')
                );

            } catch (Mage_Core_Exception $e) {
                $this->getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->getSession()->addException($e,
                    $helper->__('An error occurred while saving the testimonials item.')
                );
            }
        }

        $this->_redirect($redirectPath);
    }
}