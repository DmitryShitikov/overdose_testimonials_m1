<?php

class Overdose_Testimonials_Helper_Data extends Mage_Core_Helper_Data
{
    /**
     * Path to store config if front-end output is enabled
     *
     * @var string
     */
    const XML_PATH_ENABLED = 'testimonials/view/enabled';

    /**
     *
     * @var string
     */
    const XML_PATH_ITEMS_PER_PAGE = 'testimonials/view/items_per_page';

    /**
     * News Item instance for lazy loading
     *
     * @var Overdose_Testimonials_Model_Testimonials
     */
    protected $testimonialsItemInstance;

    /**
     *
     * @param integer|string|Mage_Core_Model_Store $store
     * @return boolean
     */
    public function isEnabled($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_ENABLED, $store);
    }

    /**
     * Return the number of items per page
     *
     * @param integer|string|Mage_Core_Model_Store $store
     * @return int
     */
    public function getNewsPerPage($store = null)
    {
        return abs((int)Mage::getStoreConfig(self::XML_PATH_ITEMS_PER_PAGE, $store));
    }

    /**
     *
     * @return Overdose_Testimonials_Model_Testimonials
     */
    public function getNewsItemInstance()
    {
        if (!$this->testimonialsItemInstance) {
            $this->testimonialsItemInstance = Mage::registry('testimonials_item');

            if (!$this->testimonialsItemInstance) {
                Mage::throwException($this->__('Testimonials item instance does not exist in Registry'));
            }
        }

        return $this->testimonialsItemInstance;
    }
}