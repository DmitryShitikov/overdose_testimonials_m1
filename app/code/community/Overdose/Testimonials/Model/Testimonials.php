<?php

class Overdose_Testimonials_Model_Testimonials extends Mage_Core_Model_Abstract
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('overdose_testimonials/testimonials');
    }
}