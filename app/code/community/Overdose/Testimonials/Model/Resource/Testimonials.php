<?php

class Overdose_Testimonials_Model_Resource_Testimonials extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize connection and define main table and primary key
     */
    protected function _construct()
    {
        $this->_init('overdose_testimonials/testimonials', 'testimonials_id');
    }
}