<?php

class Overdose_Testimonials_Model_Resource_Testimonials_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Define collection model
     */
    protected function _construct()
    {
        $this->_init('overdose_testimonials/testimonials');
    }

    /**
     * Prepare for displaying in list
     *
     * @param integer $page
     * @return Overdose_Testimonials_Model_Resource_Testimonials_Collection
     */
    public function prepareForList($page)
    {
        $this->setPageSize(Mage::helper('overdose_testimonials')->getNewsPerPage());
        $this->setCurPage($page)->setOrder('testimonials_id', Varien_Data_Collection::SORT_ORDER_DESC);
        return $this;
    }
}