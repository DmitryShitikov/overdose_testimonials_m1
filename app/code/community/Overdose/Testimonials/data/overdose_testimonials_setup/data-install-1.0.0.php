<?php
/**
 *  @var $installer Mage_Core_Model_Resource_Setup
 */
$installer = $this;

/**
 * @var $model Overdose_Testimonials_Model_Testimonials
 */
$model = Mage::getModel('overdose_testimonials/testimonials');

$dataRows = [
    [
        'title'          => 'TEST',
        'message'        => '!TEST CONTENT!',
    ],
];

foreach ($dataRows as $data) {
    $model->setData($data)->setOrigData()->save();
}