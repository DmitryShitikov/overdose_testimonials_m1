<?php

class Overdose_Testimonials_Block_List extends Mage_Core_Block_Template
{
    /**
     * @var Overdose_Testimonials_Model_Resource_Testimonials_Collection
     */
    protected $testimonialsCollection = null;

    /**
     * @return Overdose_Testimonials_Model_Resource_Testimonials_Collection
     */
    protected function _getCollection()
    {
        return  Mage::getResourceModel('overdose_testimonials/testimonials_collection');
    }

    /**
     * @return Overdose_Testimonials_Model_Resource_Testimonials_Collection
     */
    public function getCollection()
    {
        if (is_null($this->testimonialsCollection)) {
            $this->testimonialsCollection = $this->_getCollection();
            $this->testimonialsCollection->prepareForList($this->getCurrentPage());
        }

        return $this->testimonialsCollection;
    }

    /**
     * @param Overdose_Testimonials_Model_Testimonials $testimonialsItem
     * @return string
     */
    public function getItemUrl($testimonialsItem)
    {
        return $this->getUrl('*/*/view', ['id' => $testimonialsItem->getId()]);
    }

    /**
     * @return string
     */
    public function getSaveFormUrl()
    {
        return $this->getUrl('*/*/save');
    }

    /**
     * Fetch the current page for the testimonials list
     *
     * @return int
     */
    public function getCurrentPage()
    {
        return $this->getData('current_page') ? $this->getData('current_page') : 1;
    }

    /**
     * Get a pager
     *
     * @return string|null
     */
    public function getPager()
    {
        $pager = $this->getChild('testimonials_list_pager');
        if ($pager) {
            $newsPerPage = Mage::helper('overdose_testimonials')->getNewsPerPage();

            $pager->setAvailableLimit([$newsPerPage => $newsPerPage]);
            $pager->setTotalNum($this->getCollection()->getSize());
            $pager->setCollection($this->getCollection());
            $pager->setShowPerPage(true);

            return $pager->toHtml();
        }

        return null;
    }

    /**
     * Return URL for resized News Item image
     *
     * @param Overdose_Testimonials_Model_Testimonials $item
     * @param integer $width
     * @return string|false
     */
    public function getImageUrl($item, $width)
    {
        return Mage::helper('overdose_testimonials/image')->resize($item, $width);
    }
}
