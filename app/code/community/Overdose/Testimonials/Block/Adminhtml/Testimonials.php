<?php
/**
 * News List admin grid container
 *
 * @author Magento
 */
class Overdose_Testimonials_Block_Adminhtml_Testimonials extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Block constructor
     */
    public function __construct()
    {
        $this->_blockGroup = 'overdose_testimonials';
        $this->_controller = 'adminhtml_testimonials';
        $this->_headerText = Mage::helper('overdose_testimonials')->__('Manage Testimonials');

        parent::__construct();

        if (Mage::helper('overdose_testimonials/admin')->isActionAllowed('save')) {
            $this->_updateButton('add', 'label', Mage::helper('overdose_testimonials')->__('Add New News'));
        } else {
            $this->_removeButton('add');
        }
        $this->addButton(
            'testimonials_flush_images_cache',
            [
                'label'      => Mage::helper('overdose_testimonials')->__('Flush Images Cache'),
                'onclick'    => 'setLocation(\'' . $this->getUrl('*/*/flush') . '\')',
            ]
        );

    }
}