<?php

class Overdose_Testimonials_Block_Adminhtml_Testimonials_Edit_Tab_Image extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Prepare form elements
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        /**
         * Checking if user have permissions to save information
         */
        if (Mage::helper('overdose_testimonials/admin')->isActionAllowed('save')) {
            $isElementDisabled = false;
        } else {
            $isElementDisabled = true;
        }

        $form = new Varien_Data_Form();

        $form->setHtmlIdPrefix('testimonials_image_');

        $model = Mage::helper('overdose_testimonials')->getNewsItemInstance();


        $fieldset = $form->addFieldset('image_fieldset', [
            'legend'    => Mage::helper('overdose_testimonials')->__('Image Thumbnail'), 'class' => 'fieldset-wide'
        ]);

        $this->_addElementTypes($fieldset);

        $fieldset->addField('image', 'image', [
            'name'      => 'image',
            'label'     => Mage::helper('overdose_testimonials')->__('Image'),
            'title'     => Mage::helper('overdose_testimonials')->__('Image'),
            'required'  => false,
            'disabled'  => $isElementDisabled
        ]);

        Mage::dispatchEvent('adminhtml_testimonials_edit_tab_image_prepare_form', ['form' => $form]);

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('overdose_testimonials')->__('Image Thumbnail');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('overdose_testimonials')->__('Image Thumbnail');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Retrieve predefined additional element types
     *
     * @return array
     */
     protected function _getAdditionalElementTypes()
     {
         return [
            'image' => Mage::getConfig()->getBlockClassName('overdose_testimonials/adminhtml_testimonials_edit_form_element_image')
        ];
     }
}