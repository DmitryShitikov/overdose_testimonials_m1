<?php

class Overdose_Testimonials_Block_Adminhtml_Testimonials_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Initialize edit form container
     *
     */
    public function __construct()
    {
        $this->_objectId   = 'id';
        $this->_blockGroup = 'overdose_testimonials';
        $this->_controller = 'adminhtml_testimonials';

        parent::__construct();

        if (Mage::helper('overdose_testimonials/admin')->isActionAllowed('save')) {
            $this->_updateButton('save', 'label', Mage::helper('overdose_testimonials')->__('Save Item'));
            $this->_addButton('saveandcontinue', [
                'label'   => Mage::helper('adminhtml')->__('Save and Continue Edit'),
                'onclick' => 'saveAndContinueEdit()',
                'class'   => 'save',
                ], -100);
        } else {
            $this->_removeButton('save');
        }

        if (Mage::helper('overdose_testimonials/admin')->isActionAllowed('delete')) {
            $this->_updateButton('delete', 'label', Mage::helper('overdose_testimonials')->__('Delete Item'));
        } else {
            $this->_removeButton('delete');
        }

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('page_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'page_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'page_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * Retrieve text for header element depending on loaded page
     *
     * @return string
     */
    public function getHeaderText()
    {
        $model = Mage::helper('overdose_testimonials')->getNewsItemInstance();
        if ($model->getId()) {
            return Mage::helper('overdose_testimonials')->__("Edit Item '%s'",
                 $this->escapeHtml($model->getTitle()));
        } else {
            return Mage::helper('overdose_testimonials')->__('New Item');
        }
    }
}