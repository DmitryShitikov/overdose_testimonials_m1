<?php

class Overdose_Testimonials_Block_Adminhtml_Testimonials_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $model = Mage::helper('overdose_testimonials')->getNewsItemInstance();

        if (Mage::helper('overdose_testimonials/admin')->isActionAllowed('save')) {
            $isElementDisabled = false;
        } else {
            $isElementDisabled = true;
        }

        $form = new Varien_Data_Form();

        $form->setHtmlIdPrefix('testimonials_main_');

        $fieldset = $form->addFieldset('base_fieldset', [
            'legend' => Mage::helper('overdose_testimonials')->__('Testimonials Item Info')
        ]);

        if ($model->getId()) {
            $fieldset->addField('testimonials_id', 'hidden', [
                'name' => 'testimonials_id',
            ]);
        }

        $fieldset->addField('title', 'text', [
            'name'     => 'title',
            'label'    => Mage::helper('overdose_testimonials')->__('testimonials Title'),
            'title'    => Mage::helper('overdose_testimonials')->__('testimonials Title'),
            'required' => true,
            'disabled' => $isElementDisabled
        ]);

        Mage::dispatchEvent('adminhtml_testimonials_edit_tab_main_prepare_form', ['form' => $form]);

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('overdose_testimonials')->__('testimonials Info');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('overdose_testimonials')->__('testimonials Info');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }
}
