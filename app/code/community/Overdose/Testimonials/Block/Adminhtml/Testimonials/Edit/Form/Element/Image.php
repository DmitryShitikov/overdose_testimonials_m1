<?php

class Overdose_Testimonials_Block_Adminhtml_Testimonials_Edit_Form_Element_Image extends Varien_Data_Form_Element_Image
{
    /**
     * Get image preview url
     *
     * @return string
     */
    protected function _getUrl()
    {
        $url = false;

        if ($this->getValue()) {
            $url = Mage::helper('overdose_testimonials/image')->getBaseUrl() . '/' . $this->getValue();
        }

        return $url;
    }
}