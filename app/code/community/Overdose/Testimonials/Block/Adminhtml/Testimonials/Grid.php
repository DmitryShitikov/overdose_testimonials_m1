<?php
/**
 * News List admin grid
 *
 * @author Magento
 */
class Overdose_Testimonials_Block_Adminhtml_Testimonials_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Init Grid default properties
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('testimonials_list_grid');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * Prepare collection for Grid
     *
     * @return Overdose_Testimonials_Block_Adminhtml_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('overdose_testimonials/testimonials')->getResourceCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Prepare Grid columns
     *
     * @return Mage_Adminhtml_Block_Catalog_Search_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('testimonials_id', [
            'header'    => Mage::helper('overdose_testimonials')->__('ID'),
            'width'     => '50px',
            'index'     => 'testimonials_id',
        ]);

        $this->addColumn('title', [
            'header'    => Mage::helper('overdose_testimonials')->__('Title'),
            'index'     => 'title',
        ]);

        $this->addColumn('message', [
            'header'    => Mage::helper('overdose_testimonials')->__('Message'),
            'index'     => 'message',
        ]);

        $this->addColumn('action',
            [
                'header'    => Mage::helper('overdose_testimonials')->__('Action'),
                'width'     => '100px',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => [
                    [
                        'caption' => Mage::helper('overdose_testimonials')->__('Edit'),
                        'url'     => ['base' => '*/*/edit'],
                        'field'   => 'id'
                    ]
                ],
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'testimonials',
        ]);

        return parent::_prepareColumns();
    }

    /**
     * Return row URL for js event handlers
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', ['id' => $row->getId()]);
    }

    /**
     * Grid url getter
     *
     * @return string current grid url
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', ['_current' => true]);
    }
}