<?php

class Overdose_Testimonials_Block_Item extends Mage_Core_Block_Template
{
    /**
     *
     * @var Overdose_Testimonials_Model_Testimonials
     */
    protected $item;

    /**
     * Return parameters for back url
     *
     * @param array $additionalParams
     * @return array
     */
    protected function _getBackUrlQueryParams($additionalParams = [])
    {
        return array_merge(['p' => $this->getPage()], $additionalParams);
    }

    /**
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('*/', ['_query' => $this->_getBackUrlQueryParams()]);
    }

    /**
     * Return URL for resized Testimonials Item image
     *
     * @param Overdose_Testimonials_Model_Testimonials $item
     * @param integer $width
     * @return string|false
     */
    public function getImageUrl($item, $width)
    {
        return Mage::helper('overdose_testimonials/image')->resize($item, $width);
    }
}
